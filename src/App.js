import React, { Component} from 'react';
// import logo from './logo.svg';
import { BrowserRouter as Router, Switch, Route, Link} from 'react-router-dom';
import './App.css';
import About from './About';
import Contact from './Contact';
import Login from './Login';

class App extends Component  {
  render() {
    return(
      <Router>
        <div>
            <h2>Welcome to Shoppee website</h2>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
              <ul className="navbar-nav mr-auto">
                  <li><Link to={'/'} className="nav-link"> Home</Link></li>
                  <li><Link to={'/about'} className="nav-link"> About</Link></li>
                  <li><Link to={'/contact'} className="nav-link"> Register</Link></li>
                  <li><Link to={'/Login'} className="nav-link">Login</Link></li>
                  {this.props.children}
              </ul>
            </nav>
            <hr />
            <Switch>
              <Route exact path='/contact' component={Contact} />
              <Route exact path='/about' component={About} />         
              <Route exact path='/login' component={Login} />     
            </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
