import React, { Component} from 'react';
import axios from 'axios';
class Contact extends Component {
    constructor(props) {
        super(props);
        this.state = { firstname: '', lastname: '', username: '', email: '', birthdate: '', role: '', status: '', password: '' };
      }
      myChangeHandler = (event) => {
          let nam = event.target.name;
        this.setState({[nam]: event.target.value});
      }
      handleSubmit = (event) => {
        event.preventDefault();
        
       axios.post('http://localhost:8000/api/auth/signup').then(response => {
        alert("Successfully Registered!");
       }).catch(function(error) {
           console.log(error);
       })
       
      }
    render(){
        return (
            <div>
            <h1>Register to Shoppee</h1>
            <h1>{this.setState.firstname}</h1>
              <form onSubmit={this.handleSubmit}>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>First name:</label>
                      <input type="text" name="firstname" className="form-control" onChange={this.myChangeHandler}/>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Last name:</label>
                      <input type="text" name="lastname" className="form-control"  onChange={this.myChangeHandler}/>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Username:</label>
                      <input type="text" name="username" className="form-control"  onChange={this.myChangeHandler}/>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Email:</label>
                      <input type="email" name="email" className="form-control" onChange={this.myChangeHandler}/>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Birthdate:</label>
                      <input type="date" name="birthdate" className="form-control" onChange={this.myChangeHandler}/>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Role:</label>
                      <select className="form-control" onChange={this.myChangeHandler}>
                          <option>Seller</option>
                          <option>Buyer</option>
                      </select>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>mobile number:</label>
                        <input type="password" name="password" className="form-control col-md-6" onChange={this.myChangeHandler}/>
                      </div>
                    </div>
                    </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>password:</label>
                        <input type="password" name="password" className="form-control col-md-6" onChange={this.myChangeHandler}/>
                      </div>
                    </div>

                  </div><br />
                  <div className="form-group">
                    <button className="btn btn-primary" type='submit'>Register</button>
                  </div>
              </form>
        </div>
        );
    }
}

export default Contact;