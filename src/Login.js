import React, { Component} from 'react';
import axios from 'axios';
class Login extends Component{
    constructor(props) {
        super(props);
        this.state = { email: '', password: '' };
      }
      myChangeHandler = (event) => {
            let nam = event.target.name;
            this.setState({[nam]: event.target.name});
      }
      handleSubmit = (event) => {
        event.preventDefault();

        axios.post('http://localhost:8000/api/auth/login').then(response => {
            alert("Successfully Login");
           }).catch(function(error) {
               console.log(error);
               alert(error);
           })
      }
    render(){
        return(
            <div>
            <h1>Login to Shoppee</h1>
              <form onSubmit={this.handleSubmit}>
                  <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Email:</label>
                      <input type="email" name="email" className="form-control" onChange={this.myChangeHandler}/>
                    </div>
                  </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>password:</label>
                        <input type="password" name="password" className="form-control col-md-6" onChange={this.myChangeHandler}/>
                      </div>
                    </div>
                  </div><br />
                  <div className="form-group">
                    <button className="btn btn-primary" type='submit'>Login</button>
                  </div>
              </form>
        </div>
        );
    }
}
export default Login;